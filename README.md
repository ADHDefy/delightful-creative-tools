# delightful creative tools [![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/main/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful)

A curated list of delightful tools for digital creatives in a variety of mediums.

Together we collected these gems of freedom :gem: Please [delight us](https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delight-us.md) and add your own.

## Contents

- [Visual Media](#visual-media)
  - [Photo Manipulation](#photo-manipulation)
  - [Photo Management](#photo-management)
  - [Graphic Art](#graphic-art)
  - [Animation](#animation)
  - [Streaming](#streaming)
  - [Video Production](#video-production)
  - [CAD Software](#cad-software)
- [Audio](#audio)
  - [Audio Editors](#audio-editors)
  - [DAWs](#daws)
  - [Audio Plugins](#audio-plugins)
  - [Standalone Instruments](#standalone-instruments)
  - [DJ Software](#dj-software)
  - [Music Notation](#music-notation)
- [Writing](#writing)
  - [Specialized Word Processors](#specialized-word-processors)
  - [Notebooks](#notebooks)
  - [Text Editors](#text-editors)
  - [Office Suites](#office-suites)
  - [Editing](#editing)
  - [Translation](#translation)
  - [Publishing](#publishing)
- [Software Development](#software-development)
  - [Code Editors](#code-editors)
  - [UX/UI Design](#ux-ui-design)
  - [Version Control](#version-control)
- [Game Development](#game-development)
  - [Game Engines](#game-engines)
  - [AI](#ai)
  - [Asset Creation Tools](#asset-creation-tools)
  - [Public Domain Resources](#public-domain-resources)
- [Media Sharing](#media-sharing)
  - [Image Sharing](#image-sharing)
  - [Video Sharing](#video-sharing)
  - [Audio Sharing](#audio-sharing)
- [Maintainers](#maintainers)
- [License](#license)

## Visual Media

### Photo Manipulation

- [GNU Image Manipulation Program](https://www.gimp.org/) - One of the most popular Photoshop alternatives with an impressive feature set and versatility.

#### GNU Image Manipulation Program Plugins

- [PhotoGIMP](https://github.com/Diolinux/PhotoGIMP) - A patch that makes the UI and keyboard shortcuts much more like Adobe Photoshop, for those that are accustomed to that experience.
- [Resynthesizer](https://github.com/bootchk/resynthesizer) - "Texture synthesis" plugin that functions much like the content-aware filter from Photoshop.
- Both Darktable and RawTherapee (see [Photo Management](#photo-management) below) can also be used as plugins!
- [Wavelet Decompose](https://github.com/mrossini-ethz/gimp-wavelet-decompose) - Separates an image into layers based on frequency, with each layer containing a unique details from the original image. This can be helpful for skin retouching or smoothing specific parts of an image.
- [G'MIC](https://gmic.eu/) - Offers hundreds of configurable filters for digital image processing.
- [elsamuko](https://elsamuko.github.io/gimp-elsamuko/scripts) - Small collection of stylish filters and effects.
- [Beautify](https://github.com/hejiann/beautify/wiki) - Adds lots of filters with varying texture and color.
- [ReFocus](http://refocus.sourceforge.net/) - Attempts to correct the focus of an image after scanning, scaling, and other manipulations have left the pixels of the image a bit blurry.
- [ContrastFix](https://farcrydesign.com/GIMP/ContrastFix.html) - Helps you correct contrast issues in photos using masks.
- [PurpleFringe](https://farcrydesign.com/GIMP/PurpleFringe.html) - Corrects the "purple fringe" effect that occurs due to chromatic abberation.
- [Lensfun](https://lensfun.github.io/) - Corrects photographic lens errors with a large database of supported lenses.
- [Hugin](https://hugin.sourceforge.io/) - Allows you to "stitch" photos together to make a larger, panoromic image.
- [BIMP](https://alessandrofrancesconi.it/projects/bimp/) - Allows you apply edits to a whole batch of photos at once.
- [GIMPHelp Scripts](https://www.gimphelp.org/script210.html) - Over 100 miscellaneous scripts maintained by a small team.

### Photo Management

- [Darktable](https://www.darktable.org/) - Virtual lightroom and darktable for photographers, allows for processing of raw image files.
- [RawTherapee](https://rawtherapee.com/) - Like Darktable with a steeper learning curve, but with excellent HDR and tonemapping capability.
- [ART](https://bitbucket.org/agriggio/art/wiki/Home) - A popular fork of RawTherapee with the purpose of simplifying the UI while keeping functionality in tact.
- [DigiKam](https://www.digikam.org/) - Tool for sorting and organizing expansive collections of photos, with batch editing features.
- [Memories](https://memories.gallery/) - A beautiful Nextcloud app that provides a private and powerful, self-hosted photo management suite.
- [Photoprism](https://photoprism.app/) - AI-powered application for advanced photo organization and easy sharing.
- [HDRView](https://github.com/wkjarosz/hdrview) - High-dynamic range image viewer for viewing and comparing images with support for many file types, 10-bit displays, and Apple EDR.
- [Siril](https://siril.org/) - Advanced astronomical image processing for space photographers. Provides calibration, enhancement, and more.
- [Upscayl](https://www.upscayl.org/) - Excellent AI image upscaler.
- [Hugin](https://hugin.sourceforge.io/) - Panoramic imaging toolchain for stitching photos together.
- [Curtail](https://github.com/Huluti/Curtail) - Handy image compressor for Linux.
- [pngquant](https://pngquant.org/) - Efficient command line utility for compressing PNG files.

### Graphic Art

- [Krita](https://krita.org/en/) - Advanced painting program for artists of any skill level. It's cross-platform, feature-rich, customizable, and intuitive to use. It even provides some photo manipulation and basic animation capability.
  - [David Revoy's 2021 Krita Brushes Bundle](https://www.davidrevoy.com/article854/krita-brushes-2021-bundle), his [2023 Brushes Bundle](https://www.davidrevoy.com/article953/krita-brushes-2023-01-bundle), and his [Speed Painting Brush Bundle](https://www.davidrevoy.com/article893/speedpaintings-brushes-tips-and-4-timelapses-krita-5) are all packs of excellent custom brushes for Krita by artist David Revoy.
  - [Wavenwater Concept Art Brushes Bundle](https://krita-artists.org/t/wavenwater-concept-art-traditional-medium-beta/60632) replicates a few traditional art tools inspired by concept art from the Disney renaissance era.
  - [GDQuest's Free Krita Brushes for Game Artists](https://github.com/GDQuest/krita-free-brushes) offers several professional brushes made for game artists, but would work well for concept artist and other illustrations, too.
  - [Realistic Copic Marker-Like Brushes Bundle](https://krita-artists.org/t/realistic-copic-marker-like-brushes/41203) gives you authentic looking, Copic marker inspired brushes.
  - [ED Free Hair Brushes and Blenders Brushes Bundle](https://krita-artists.org/t/ed-free-hair-brushes-and-blenders/46808) offers 12 custom brushes for painting hair.
  - [Krita brushes thumbnail generator](https://github.com/NathanLovato/krita-brush-thumbnails-generator) is a tool for generating layered thumbnails for your own custom brushes.
  - [Shortcut-Composer](https://github.com/wojtryb/Shortcut-Composer) allows you to make custom pie menus in Krita.
- [Inkscape](https://inkscape.org/) - Excellent vector art program for designers and illustrators.
- [Graphite](https://github.com/GraphiteEditor/Graphite) - Alpha-stage graphics editor for 2D raster and vector art, powered by a node graph compositing engine. It is lightweight and runs in your web browser.
- [MyPaint](https://mypaint.org/) - Simple paint program with a distraction-free fullscreen mode and an infinite canvas. No longer actively maintained.
- [Our Paint](https://www.wellobserve.com/index.php?post=20221222155743) - "Featurless" painting tool that is heavily programmable and customizable with a borderless canvas.
- [Drawpile](https://drawpile.net/) - Drawing program with a canvas that can be shared in real-time by multiple users over the internet. Seemingly no longer maintained, but still a lot of fun.
- [WBO](https://wbo.ophir.dev/) - Another tool similar to Drawpile, with real-time online "whiteboard" drawing.
- [Rickrack](https://github.com/eigenmiao/Rickrack) - Fun and user-friendly color palatte generator.
- [BeeRef](https://github.com/mini-ninja-64/beeref) - Simple but extremely useful reference image viewer, recently forked and once again receiving updates.
- [Silky Shark](https://github.com/stoicshark/silkyshark) - Mouse and tablet stabilizer, excellent accessibility tool for those that struggle keeping a steady hand.
- [Font Manager](http://fontmanager.github.io/) - Made to help users manage system fonts easily on Linux, and it's a great tool for graphic designers.
- [MFEKglif](https://github.com/MFEK/glif) - Typeface editor for making custom fonts.

### Animation

- [Blender](https://www.blender.org/) - Incredible software that offers professional capability in 3D modeling, 3D animation, 2D animation, CGI/VFX, and more.
  - [agmmnn's Awesome Blender repo](https://github.com/agmmnn/awesome-blender#Add-ons-) has a massive list of open source plug-ins that could help improve your workflow in Blender.
  - [MakeHuman](http://www.makehumancommunity.org/) is an additional piece of software for creating rigged, humanoid character models to import directly into Blender.
  - [SheepIt Client](https://github.com/laurent-clouet/sheepit-client) is an open-source client for the free, distributed SheepIt render farm.
  - [Sequence Menu](https://github.com/tin2tin/sequence_menu) adds an enhanced UX and new features for the Blender Video Sequence Editor.
  - [BlenderBIM](https://blenderbim.org/) & [ifc-git](https://github.com/brunopostle/ifc-git) work together to allow you to browse branches and revisions of an IFC file in a git repo, or create a new repo.
  - [Audacity Tools for Blender](https://github.com/tin2tin/audacity_tools_for_blender/) allows you to use Blender's video sequence editor in sync with Audacity for better audio filtering and more.
  - [Flatterer](https://stuvel.eu/software/flatterer/) turns 3D models into a set of 2D shapes for laser cutting.
  - See [Public Domain Resources](#public-domain-resources) below for pre-made textures, 3D models, and more.
- [Tooll 3](https://github.com/still-scene/t3) - Amazing realtime animation toolkit for motion graphic artists that can be used in audio-reactive VJ content, automating keyframe animation, development of fragment or compute shaders, and much more.
- [OpenToonz (Morevna Edition)](https://morevnaproject.org/opentoonz/) - 2D animation program geared toward traditional frame-by-frame animation with support for plug-ins that utilize deep learning to add impressive visual effects, based on software adopted and customized by the incomparable Studio Ghibli. The Morevna Edition includes a more intuitive UI, brushes from MyPaint, an advanced color selector, and more.
  - [DWANGO's Effect Plugins](https://github.com/opentoonz/dwango_opentoonz_plugins) is a collection of visual effect plugins for OpenToonz
  - [KumoWorks](https://github.com/opentoonz/kumoworks) is a cloud rendering tool for OpenToonz animations
- [enve](https://maurycyliebner.github.io/) - Impressive 2D animation and motion graphics software with support for both raster and vector art, sound, and video files.
- [Synfig Studio](https://www.synfig.org/) - 2D animation software that appears to be no longer actively maintained, though it is feature-rich and provides support for bone rigging, cut-out animation, and traditional frame-by-frame.
- [Pencil2D](https://www.pencil2d.org/) - Minimalistic, cross-platform 2D animation software that allows for easy switching between raster and vector workflows.
- [FreeMoCap](https://freemocap.org/) - Advanced but usable motion capture framework for everyone!
- [Infinigen](https://infinigen.org/) - A powerful tool for procedural generation of infinite, photorealistic worlds with real geometry.
- [Papagayo-NG](https://github.com/morevnaproject-org/papagayo-ng) - Tool for generating 2D lip sync animation. The result typically needs some manual tweaking, but it takes a lot of work out of the process. It works with Synfig, OpenToonz, and Krita, among others.
- [Storyboarder](https://wonderunit.com/storyboarder/) - Storyboard and animatic software for easy visualization of your film/animation project during the pre-production phases.
- [MoonRay](https://openmoonray.org/) - Dreamwork's Monte Carlo Path Tracer renderer used in their animated films.
- [Pixie](https://www.renderpixie.com/pixie_renderer.html) - Photorealistic, ray-traced renderer similar to Pixar's proprietary RenderMan.
- [LuxCoreRender](https://luxcorerender.org/) - Physically-based and unbiased rendering engine.
- [RenderChan](https://morevnaproject.org/renderchan/) - Rendering manager for 2D animation projects.
- [Universal Scene Description (USD)](https://github.com/PixarAnimationStudios/USD) - Format made by Pixar that combines lots of scene data and allows for real-time collaboration between artists across multiple applications.
- [Wick Editor](https://www.wickeditor.com) - Adobe Animate-inspired animation tool with bulit-in scripting.

### Streaming

- [OBS Studio](https://obsproject.com/) - A brilliant piece of software for livestreaming and video recording.
  - [OBS Background Removal Plugin](https://github.com/royshil/obs-backgroundremoval) creates a virtual green-screen for portrait background removal without needing an actual green-screen.
- [NoiseTorch](https://github.com/noisetorch/NoiseTorch) - Real-time mic noise suppresion for Linux.

### Video Production

- [kdenlive](https://kdenlive.org/) - Video editor with multi-track capability, support for a wide varitey of video and audio formats, and a suite of useful production tools. It's been in active development since 2002!
- [Shotcut](https://shotcut.org/) - Video editor with professional audio processing features, lots of video effects, and a customizable interface with undockable panels. It also boasts support for some video-related hardware.
- [Pitivi](https://www.pitivi.org/) - Video editor with a focus on design and usability, with natural integration into the GNOME desktop environment.
- [Flowblade](https://jliljebl.github.io/flowblade/) - Video editor with a multitude of included tools, boasting stability and a speedy workflow.
- [Olive Video Editor](https://www.olivevideoeditor.org/) - Video editing software with node-based compositing built-in and fast disk cache performance.
- [OpenShot](https://www.openshot.org/) - Simple, bare-bones video editing akin to Windows Movie Maker or iMovie.
- [Avidemux](http://avidemux.sourceforge.net/) - Small but mighty. Ideal for quick edits, filtering, and encodes.
- [Gyroflow](https://gyroflow.xyz/) - Gyro-based video stabalization.
- [OpenColorIO](https://opencolorio.org/) - Color grading/management tool for use with Olive Video Editor, Blender, Krita, and more.
- [Natron](https://natrongithub.github.io/) - Professional compositing software for Motion Graphics and VFX.
- [Open RV](https://github.com/AcademySoftwareFoundation/OpenRV) - Award-winning image and sequence viewer for VFX artists and animators.
- [xSTUDIO](https://www.dneg.com/xstudio/) - Similar to Open RV, this is another media playback and review program for film and animation.
- [Handbrake](https://handbrake.fr/) - Powerful, cross-platform video transcoder.
- [Video Hub App](https://videohubapp.com/en/) - Brilliant tool for organizing and managing video libraries, great for filmmaking and media consumption. Pre-built binaries are available for $5 (most of which [goes to charity](https://github.com/whyboris/Video-Hub-App#download-now)), or you can [build it from source](https://github.com/whyboris/Video-Hub-App) for free!

### CAD Software

- [FreeCAD](https://www.freecadweb.org/) - 3D parametric modeler for product design, mechanical engineering, and architecture. Features Python integration, OpenSCAD integration, and 2D drafting.
- [QCAD](https://www.qcad.org/en/) - Another excellent CAD program with layers, grouping, 60+ tools, a part library with 4800+ CAD parts, and more.
- [OpenSCAD](https://openscad.org/) - Code-based, non-interactive 3D modeling workbench.
- [LibreCAD](https://librecad.org/) - Lightweight 2D drafting software.
- [OpenVSP](http://openvsp.org/) - Parametric aircraft geometry tool for designing functional air vehicles.
- [Avogadro](https://avogadro.cc/) - Molecular editor and visualizer for chemical engineers.
- [MolView](https://molview.org/) - Web-based molecular editor and visualizer.
- [topologicpy](https://github.com/wassimj/topologicpy) - Spacial modelling and analysis for architecture, engineering, and construction.
- [Mayo](https://github.com/fougue/mayo) - 3D CAD model viewer and converter.

## Audio

### Audio Editors 

- [Audacity](https://www.audacityteam.org/) - Simple, intuitive multi-track recorder and waveform editor. It has been around since 2000 and remains an exceptionally popular tool for audio recording and editing. 
- [Tenacity](https://tenacityaudio.org/) - A fork of Audacity from an older version (3.0.2), now taking the project in a new direction.
- [Wavacity](https://wavacity.com) - Another fork of Audacity, but this time ported to the web browser!  

#### Music Source Separation Libraries
- [Open-Unmix](https://github.com/sigsep/open-unmix-pytorch) - CLI program that will attempt to separate individual tracks from an audio recording using PyTorch.
- [Spleeter](https://github.com/deezer/spleeter) - Another music source separator developed by Deezer developers. 
- [Demucs](https://github.com/facebookresearch/demucs) - Yet another, but developed by Meta researchers (I know, I'm sorry, but it is entirely FOSS).

#### Metadata Management
- [Tone](https://github.com/sandreas/tone) - Audio tagger for dumping and editing metadata from multiple audio formats, such as FLAC, MP3, M4B, and more.

### DAWs

*Digital Audio Workstations*

- [Ardour](https://ardour.org/) - Full-featured, cross-platform, multi-track workstation & MIDI sequencer with support for VST, LV2, and LADSPA plugins. Best option for working with audio tracks.
- [Qtractor](https://qtractor.org/) - Multi-track recorder/editor with a powerful MIDI sequencer for Linux. Supports VST, LV2, DSSI, and LADSPA plugins. Not as capable as Ardour in working with audio tracks, but much better at handling MIDI tracks.
- [MusE](https://github.com/muse-sequencer/muse) - Complete multitrack DAW for Linux that handles both MIDI and audio tracks.
- [Zrythm](https://www.zrythm.org/en/index.html) - An in-development automated and convenient workstation for music production.
- [Helio](https://helio.fm/) - Remarkably intuitive and concise sequencer with helpful visualization tools.
- [LMMS](https://lmms.io/) - Ideal workstation for beatmaking or a loops-based workflow, like you may find in FL Studio or Ableton Live, though it does not feature native audio recording.
- [ossia score](https://ossia.io/) - Intermedia sequencer with mixed node-based and timeline workflow.
- [Ensembles](https://github.com/SubhadeepJasu/Ensembles) - Digital workstation for arranging music for live performance, not targeted toward studio recording.
- [Rosegarden](https://www.rosegardenmusic.com/) - MIDI sequencer and editing tool for composers with classical music notation built-in.

### Audio Plugins

#### Plugin Hosts

- [Carla](https://kx.studio/Applications:Carla) - Non-linear plugin host for running plugins outside the DAW, using Windows VSTs on Linux, and more.
- [Yabridge](https://github.com/robbert-vdh/yabridge) - Seamless support for both 64-bit and 32-bit VSTs (VST2 & VST3) on 64-bit Linux hosts.
- [AudioGridder](https://audiogridder.com/) - Allows users to offload DSP processing to remote computers to save system resources when using intensive plugins.
- [Element](https://github.com/kushview/Element) - VST plugin that is itself a modular, node-based plugin host that allows you to chain together plugins to create powerful new sounds.

#### Packs

- [Calf Studio Gear](https://calf-studio-gear.org/) - Extensive pack of LV2 plugins including instruments and effects in modulation, delay, dynamics, EQ, and more.
- [Socalabs Plugins](https://socalabs.com/) - Collection of useful plugins including a great reimplementation of mverb, some basic effects, several synths inspired by retro video game consoles, utility plugins, and more. 
- [DPF-Plugins](https://github.com/DISTRHO/DPF-Plugins) - Small collection of plugins by DISTRHO in LADSPA, DSSI, LV2, VST2 and VST3 formats.
- [DISTRHO Ports](https://github.com/DISTRHO/DISTRHO-Ports) - Collection of open-source plugin ports to Linux, including the popular TAL-Noisemaker, Vitalium, mverb, and more.
- [IEM Plug-in Suite](https://plugins.iem.at/) - Plugin suite comprised of a great multi-band compressor, encoders, decorders, and visualization tools.
- [Zam-Plugins](https://www.zamaudio.com/?p=976) - Several audio plugins including a really nice dynamic EQ and more, all in LV2 or VST formats.
- [Invada Studio](https://launchpad.net/invada-studio/) - Pack of basic effects, filters, and more.
- [x42 MIDI Filter Collection](https://x42-plugins.com/x42/x42-midifilter) - More than 25 MIDI data filters.
- [Linux Studio Plugins (LSP)](https://lsp-plug.in/) - Suite of excellent plugins made for Linux users. 

#### Synthesizers

- [Cardinal](https://github.com/DISTRHO/Cardinal) - FOSS version of VCV Rack 2 that runs as a plugin in your DAW, or even [in your browser](https://cardinal.kx.studio/)!
- [Helm](https://tytel.org/helm/) - Versatile polyphonic synthesizer in multiple plugin formats or a standalone app.
- [Surge](https://surge-synthesizer.github.io/) - Subtractive hybrid synth that seemingly does it all.
- [Odin 2](https://www.thewavewarden.com/odin2) - A monstrous 24-voice polyphonic synth fit for the gods.
- [ZynAddSubFX](https://github.com/zynaddsubfx/zynaddsubfx) - Polyphonic synth with additive, subtractive, and pad modes.
- [Orchestools Genesyn 2](https://github.com/ilirbajri/ORCHESTOOLS-GENESYN-2) - Additive synth with 4 generators and 100 presets to play with.
- [Sorcer](https://github.com/openAVproductions/openAV-Sorcer) - LV2 wavetable synth primarily for EDM producers.
- [Geonkick](https://geonkick.org/) - Fantastic modern percussion synthesizer.
- [WeirdDrums](https://github.com/dfilaretti/WeirdDrums) - Small but highly tweakable synthesizer for techno/EDM drum sounds. 
- [Dexed](https://github.com/asb2m10/dexed) - Synth plugin inspired by the Yamaha DX7. 
- [DSP56300](https://dsp56300.wordpress.com/) - Emulating late '90s/early '00s VA Synths based on the Motorola 56300 chip, such as the Virus B and Virus C.
- [Digits](http://www.extentofthejam.com/) - Fantastic sounding Casio CZ synth emulation.
- [OPNPlug](https://github.com/jpcima/ADLplug) - FM Chip synthesizer, inspired by the YM2612 found in the Sega Genesis/MegaDrive and some classic arcade cabinets. Great for chiptunes.
- [SoyBoy](https://github.com/t-sin/soyboy-sp.vst3) - Emulating the sounds of the Game Boy.

#### Virtual Instruments

- [DrumGizmo](https://drumgizmo.org/wiki/doku.php) - Drum sampler plugin with great samples and support for eDrum triggers (with positional sensitivity in the works).
- [x42 AVL Drums](https://x42-plugins.com/x42/x42-avldrums) - Acoustic drum sampler plugin, now with three great sounding kits.
- [Hydrogen](http://hydrogen-music.org/) - MIDI drum machine that can be used on its own or as a sequencer and trigger for external virtual drums (like DrumGizmo) and other virtual instruments.
- [x42 setBfree](https://x42-plugins.com/x42/setBfree) - Classic tonewheel organ, great for jazz, blues, and more.
- [Orchestools](https://musictop69.wixsite.com/orchestools/orchestools-two) - This is actually 4 plugins: Strings, Brass, Winds, and Percussion. Together, you will have all of the sounds of a symphony orchestra at your disposal.

#### Sampling

- [Drops](https://github.com/clearly-broken-software/drops) - For easily processing samples and cutting loops.
- [ninjas2](https://github.com/clearly-broken-software/ninjas2) - Another easy sample slicer.
- [SooperLooper](https://sonosaurus.com/sooperlooper/) - Real-time live looping for Linux and MacOS users.

#### Dynamics

- [EQ10Q](http://eq10q.sourceforge.net/) - Powerful and flexible 10-band parametric equalizer with filters.
- [Luftikus](https://lkjbdsp.wordpress.com/luftikus/) - Maag-inspired, analogue style EQ.
- [WSTD EQ](https://wasted.audio/software/wstd_eq) - 3-band EQ that adds a bit of "crunch" to the sound.
- [WSTD 3Q](https://wasted.audio/software/wstd_3q) - Same as the WSTD EQ, only with three seperate outputs. Needs a plugin host that supports port-groups.
- [Squeezer](https://github.com/mzuther/Squeezer) - Flexible, all-purpose compression.
- [abGate](https://abgate.sourceforge.io/) - Noise gate in LV2 format.
- [HiLoPass](https://github.com/teragonaudio/HiLoFilter) - Simple high-pass/low-pass filter that is controlled with just one knob.

#### Delay and Reverb

- [CHOW Matrix](https://github.com/Chowdhury-DSP/ChowMatrix) - Infinitely growable, complex delay effect with lots of options.
- [Cocoa Delay](https://tesselode.itch.io/cocoa-delay) - Nice, warm delay plugin in Windows VST format.
- [Regrader](https://github.com/igorski/regrader) - Degenerative delay where each repeat degrades in resolution.
- [mverb](https://github.com/martineastwood/mverb) - Great sounding, simple reverb plugin.
- [CloudSeed](https://github.com/ValdemarOrn/CloudSeed) - Vast and beautiful ambient reverb in VST format.
  - [Aether](https://github.com/Dougal-s/Aether) is a fork of Cloud Seed available in LV2 format. 
- [Teufelsberg Reverb](https://www.balancemastering.com/blog/balance-audio-tools-free-teufelsberg-reverb-plugin/) - 6 distinctive reverb sounds captured at the Berlin surveillance tower.
- [Dragonfly Reverb](https://michaelwillis.github.io/dragonfly-reverb/) - A pack of reverbs including hall, plate, room, and reflections effects.

#### Modulation

- [YK Chorus](https://github.com/SpotlightKid/ykchorus) - Chorus effect based on TAL's DSP code.
- [WSTD FLANGR](https://wasted.audio/software/wstd_flangr) - Basic flanger plugin with a wide range of sound.
- [CHOW Phaser](https://github.com/jatinchowdhury18/ChowPhaser) - Very nice phhaser plugin available in both mono and stereo.

#### Distortion / Saturation

- [CHOW](https://github.com/Chowdhury-DSP/CHOW) - Distortion plugin for heavy degredation.
- [C1Bitcrusher](https://github.com/datajake1999/C1Bitcrusher) - Authentic sounding bitcrusher in VST2 format.
- [CHOW Tape Model](https://github.com/jatinchowdhury18/AnalogTapeModel) - Digital emulation of analogue, reel-to-reel tape saturation.
- [Roth-AIR](https://github.com/danielrothmann/Roth-AIR) - Combines multiband compression and gentle saturation on high frequencies to add a crispy, airy presence to your tracks that is great for vocals, synths, strings, and more.

#### Vocal Processing

- [x42 Auto Tune](https://x42-plugins.com/x42/x42-autotune) - Pitch correction.
- [Autotalent](http://tombaran.info/autotalent.html) - Pitch correction.

#### Guitar Effects

- [NAM (Neural Amp Modeler)](https://www.neuralampmodeler.com/) - A truly impressive modeler that uses deep learning to replicate the sound of nearly any guitar amp or effect pedal.
  - [ToneHunt](https://tonehunt.org/) - Very large online repository of NAM models created by the community.
- [guitarix](https://guitarix.org/) - Multi-format virtual amplifier.
- [GxPlugins](https://github.com/brummer10/GxPlugins.lv2) - Suite of LV2 plugins intended to be a standalone expansion to guitarix.
- [BYOD (Build-Your-Own-Distortion)](https://chowdsp.com/products.html) - Allows the user to create custom guitar effects from distortion and tone-shaping effect emulation in a node-based UI.
- [SmartGuitarAmp](https://github.com/GuitarML/SmartGuitarAmp) - Uses machine learning to simulate real-world guitar amplifiers.
- [SmartGuitarPedal](https://github.com/GuitarML/SmartGuitarPedal) - Uses maching learning to simulate real-world guitar effect pedals.

#### Misc Effects

- [BinAural VST](https://github.com/twoz/binaural-vst) - Mono-to-stereo plugin that allows you to position sound in 3D space.
- [Wolf Shaper](https://github.com/wolf-plugins/wolf-shaper) - Waveshaper with a graphic editor.
- [ArgotLunar](https://mourednik.github.io/argotlunar/) - Granular synthesis with lots of flexibility.
- [AirWindows](https://github.com/airwindows/airwindows) - Incredibly subtle, "subliminal" sound texturing plugin.
- [PaulXStretch](https://sonosaurus.com/paulxstretch/) - Extreme time-stretching plugin.
- [Flutterbird](https://tesselode.itch.io/flutterbird) - Automated pitch and volume fluxuation.
- [x42 Phase Rotate](https://x42-plugins.com/x42/x42-phaserotate) - Rotates the phase of a signal up to 180 degrees.
- [x42 Whirl Speaker](https://x42-plugins.com/x42/x42-whirl) - Imitates the sound of Don Leslie's electromechanical rotating speaker.
- [plugdata](https://plugdata.org/) - Visual programming UI based on pure-data for audio experimentation. Can be used as a plugin or standalone.

#### Mastering

- [x42 Digital Peak Limiter](https://x42-plugins.com/x42/x42-limiter) - Tool for preventing clipping during mixing and mastering.
- [x42 Meter Collection](https://x42-plugins.com/x42/x42-meters) - Audio meters for dynamic range, phase, LUFS and more.
- [ReFine](https://lkjbdsp.wordpress.com/refine/) - The final coat of paint for your new song, allowing you to dial in more warmth, space, and punch to individual tracks or the overall mix.

### Standalone Instruments

#### Synthesizers

- [VCV Rack](https://vcvrack.com/) - An insanely powerful and configurable modular synth with tons of possible applications.
  - [VCVRack Library](https://github.com/VCVRack/library) is a collection of plugins for VCVRack.

#### Trackers

- [Bosca Ceoli](https://terrycavanagh.itch.io/bosca-ceoil) - Tracker that comes pre-loaded with over one hundred MIDI and chiptune instrument sounds.
- [MilkyTracker](https://milkytracker.org/) - Chiptune synthesizer and sequencer.
- [klystrack](https://github.com/kometbomb/klystrack) - Simple but powerful four-track synth and sequencer.
- [SunVox](https://warmplace.ru/soft/sunvox/) - Small modular synth for a variety of devices.
- [Schism tracker](https://schismtracker.org/) - Open source re-implementation of Impulse Tracker.
- [hUGETracker](https://github.com/SuperDisk/hUGETracker) - Tracker for Game Boy homebrew and chiptune composers.
- [FamiStudio](https://famistudio.org/) - Tracker for NES/Famicom homebrew and chiptune composers.
- [GoatTracker 2](https://sourceforge.net/projects/goattracker2/) - Tracker for Commodore 64 homebrew and chiptune composers.

### DJ Software

- [Mixxx](https://mixxx.org/) - Community-driven software with dozens of features for newbie DJs and master turntableists alike.

### Music Notation

- [MuseScore](https://musescore.org/en) - Create and print your own sheet music in multiple formats (including classical, jazz chord charts, tableture, vocal arrangements, and more), with a clean UI and support for MIDI keyboard input.
- [LilyPond](https://lilypond.org/) - Flexible software for making beautiful, printable sheet music. It is extremely customizable and can be expanded for practically any kind of notation you might need.
- [Chordly](https://chordly.co.uk/) - Easily create simple chord charts with lyrics and transpose chords as needed.

## Writing

### Specialized Word Processors

- [Bibisco](https://bibisco.com/) - Novel writing application with additional tools to aid in character development and world building, organizational featues, and the capability to anaylize your novel and provide helpful data at a glance.
- [Manuskript](https://www.theologeek.ch/manuskript/) - Software for storytellers who are also planners, offering a detailed, hierarchal outliner and "novel assistant" to help you think it all out before you've started writing, as well as a distraction-free text editor.
- [Kit Scenarist](https://kitscenarist.ru/en/index.html) - Cross-platform screenwriting application with a handy research module for easy reference of research material and virtual cards for mapping out your story structure.
- [GitBook](https://www.gitbook.com/) - Software that takes advantage of Git's version control system for technical writing and documentation.
- [Twine](https://twinery.org/) - Tool for developing non-linear, interactive fiction.

### Text Editors

- [ghostwriter](https://ghostwriter.kde.org/) - Text editor that allows you to use Markdown to create documents in various formats, all within a clean and customizable interface.
- [Emacs](https://www.gnu.org/software/emacs/) - Advanced, power-user text editor with content-aware editing modes, built-in documentation, and much, much more.
- [Spacemacs](https://www.spacemacs.org/) - Alternative to Emacs with a VIM-like featureset incorporated into the editor.
- [espanso](https://espanso.org/) - A "text expander" that allows you to code custom keywords to make your typing more efficient.

### Notebooks

- [Joplin](https://joplinapp.org/) - Cross-platform note-taking app with end-to-end encrypted note syncing between devices and a handful of other great features.
- [Turtl](https://turtlapp.com/) - Secure notebook app with an emphasis on collaboration.
- [Journal](https://journalapp.nl/) - Self-hostable, private journaling webapp.
- [EteSync Notes](https://www.etesync.com/) - Self-hostable, encrypted notes app for mobile and web.

### Office Suites

- [LibreOffice](https://www.libreoffice.org/) - A truly free and complete office suite for everyone.
  - [Collabora Online](https://www.collaboraoffice.com/collabora-online/) is a self-hostable online version of LibreOffice with collaborative editing. It needs a document management platform such as [Nextcloud](https://nextcloud.com) - many Nextcloud providers offer some version of Collabora Online.
- [OnlyOffice](https://www.onlyoffice.com/) - Modern, full-featured Office alternative for collaborators. Excellent compatability with Microsoft Office documents and can also integrate with Nextcloud.
	- [CryptPad](https://cryptpad.org/) is a self-hostable, cloud-based office solution that integrates OnlyOffice tools in the web browser, similar to Office365, and includes other features like Kanban and a real-time whiteboard aapplication.

### Editing

- [LanguageTool](https://languagetool.org/) - Grammer, style, and spell checker in the form of a desktop app, webapp, browser add-on, office-plugin, and more. Supports several common languages and counting.
- [Expresso](https://www.expresso-app.org/) - Webapp for fast neural network analysis of your text style to provide useful feedback metrics to English writers.

### Translation

- [LibreTranslate](https://libretranslate.com/) - Machine translation API for text and documents that is self-hostable, can be used offline, and has a free webapp.

### Publishing

- [LaTeX](https://www.latex-project.org/) - Document and book formatting software for anything from scientific research papers to high fantasy novels.
- [Scribus](https://www.scribus.net/) - Versatile desktop publishing toolset for finalizing print-ready books, magazines, and more. 
- [gscan2pdf](https://gscan2pdf.sourceforge.net/) - Easily make PDFs or DjVus from scanned documents.

## Software Development

### APIs

- [Public APIs List on Github](https://github.com/public-apis/public-apis) - Very long, organized list of free public APIs for your development projects.

### Code Editors

- [VSCodium](https://vscodium.com/) - Forked version of Microsoft's open source VSCode, relicensed under the MIT license and without telemetry.
- [Geany](https://geany.org/) - Programmer's text editor and IDE that is lightweight, customizable, and supports many plugins.
- [Lua Carousel](https://akkartik.itch.io/carousel) - Lightweight environment for programming on phones, tablets and computers.
- [Notepad++](https://notepad-plus-plus.org/) - Simple notepad and source code editor for Windows.

### UX/UI Design

- [penpot](https://penpot.app/) - Web-based design and prototyping platform made for cross-domain teams, a noteworthy alternative to Figma.
- [Pencil Project](https://pencil.evolus.vn/) - Cross-platform tool for making GUI prototypes.
- [Alva](https://github.com/meetalva/alva) - Tool for making "living" prototypes with working code components.
- [Excalidraw](https://excalidraw.com/) - Generic, intuitive, in-browser sketching tool for quick concepting.
- [Akira](https://github.com/akiraux/Akira) - Offline generic sketching tool, also great for concepting.
- [Icon Shelf](https://icon-shelf.github.io/) - Icon manager for web developers where all icons are copyable as code.

### Version Control

- [Meld](https://meldmerge.org/) - Offline tool for comparing and/or merging files and folders on your sytem. Extremely useful for general organization and project management, and can be useful for lots of tasks outside of software development, too.

## Game Development

### Game Engines

- [Godot](https://godotengine.org/) - Impressive engine for making professional games in 2D or 3D. Supports multiple languages, including C++, C#, and the developer's own GDScript. It also offers visual scripting tools, VCS integrations (git, etc.), and can deploy to any modern desktop or mobile OS (or even game consoles, with third-party support).
  - [Godot's own Awesome Godot repo](https://github.com/godotengine/awesome-godot) has a wide variety of free plugins listed.
  - [Keychain](https://github.com/Orama-Interactive/Keychain) is a plugin that provides a control re-mapping system for the players of your game.
- [Armory3D](https://armory3d.org/) - Powerful 3D game engine that integrates with Blender. Made by the team behind ArmorPaint and ArmorLab.
- [GDevelop](https://gdevelop.io/) - Easy-to-learn 2D game engine that requires no coding knowledge and includes all the assets one would need to start learning game development. It also offers live previews of your game, easy integration with third-party tools, JavaScript support for building more complex games, and extensive tutorials on their website.
  - [GDevelop-extensions](https://github.com/GDevelopApp/GDevelop-extensions) is a GitHub repo of open-source, community made extensions for GDevelop.
- [Bevy](https://bevyengine.org/) - Exciting cross-platform, data-driven game engine for Rust developers, currently in active development, with a growing assortment of community plugins and assets available.
  - [The Unofficial Bevy Cheatbook](https://bevy-cheatbook.github.io/setup/unofficial-plugins.html) has a list of excellent plugins, in addition to loads of instruction and documentation.
- [libGDX](https://libgdx.com/) - Cross-platform, Java-based game engine behind many popular indie games. Offers a unified API and extensive third-party plugin support.
- [Panda3D](https://www.panda3d.org/) - Engine for real-time 3D games and other applications (visualizations, simulations, etc.).
- [LÖVE](https://love2d.org/) - Framework for building 2D games easily with the Lua scripting engine.
- [Superpowers](https://superpowers-html5.com/index.en.html) - Game engine made with ease of use and real-time collaboration in mind, using TypeScript for game logic. It includes free assets and allows users to publish to multiple platforms, including web browsers.
- [Spring](https://springrts.com/) - An engine specifically built for making custom RTS games with Lua scripting.

### AI

- [Fluent Behavior Tree](https://github.com/ashleydavis/Fluent-Behaviour-Tree) - Behaviour tree library written in C# with a fluent API and good documentation.
- [SimpleAI](https://github.com/mgerhardy/simpleai/) - Lightweight behavior tree written in C++ with optional LUA bindings.
- [Bonsai](https://github.com/Sollimann/bonsai) - Behavior tree written in Rust with nice documentation.
- [Modular Behavior Tree](https://github.com/telcy/modular-behavior-tree) - Behavior tree in JavaScript that provides basic nodes and provides the ability to write and add your own.

### Asset Creation Tools

#### Pixel Art and 2D Sprites

*See also: [Graphic Art](#graphic-art)*

- [Pixelorama](https://orama-interactive.itch.io/pixelorama) - Suite of tools for making sprites, backgrounds, tiles, pixel art animations, etc.
- [Piskel](https://www.piskelapp.com/) - Editor for animated sprites and pixel art, works in-browser and as a standalone application.
- [LibreSprite](https://libresprite.github.io/) - Totally free fork from the last GPLv2 commit of the popular open source [Aseprite](https://www.aseprite.org/) pixelart program. While it's missing some newer features and bugfixes from the current versions of Aseprite, the developers are working on a ground-up [rewrite](https://github.com/LibreSprite/Dotto) with exciting new features that is worth keeping an eye on.
- [EZSpriteSheet](https://github.com/z64me/EzSpriteSheet) - Spritesheet packer that can even import from GIFs and WEBPs.

#### Voxel Art and 3D Models

*See also: [Animation](#animation)*

- [ArmorPaint](https://armorpaint.org/) - Node-based texture painting for 3D models. Can be freely built from [source](https://github.com/armory3d/armorpaint).
- [ArmorLab](https://armorlab.org/) - AI-powered texture authoring from text or photo references.
- [Goxel](https://goxel.xyz/) - Powerful, cross-platform editor for voxel-based 3D models with support for layers, unlimited canvas size, and more.
- [DEM.Net Elevation API](https://elevationapi.com/) - Live 3D terrain generation from realworld data.
- [Capsaicin](https://gpuopen.com/capsaicin/) - AMD's Experimental real-time rendering framework for DirectX graphics development and research.

#### Level Design and Maps

- [Tiled](https://www.mapeditor.org/) - Tilemap/level editor supported by tons of game development frameworks.
- [LDtk](https://ldtk.io/) - 2D level editor with an intuitive UI, simple export, and many other modern features.

#### Sound Effects and Music

*See also: [Audio](#audio)*

- [ChipTone](https://sfbgames.itch.io/chiptone) - Easy to use SFX synthesizer with tweakable example presets, with a sampler and sequencer built-in.
- [JFXR](https://jfxr.frozenfractal.com/) - Webapp for generating sound effects for your games, with semi-randomized presets.
- [BeepBox](https://www.beepbox.co/) - A simple browser-based chiptune synth and sequencer for game music loops and SFX.
- [LabChirp](http://labbed.net/software/labchirp/) - Versatile sound effect generator with great documentation.

#### Dialogue

- [Yarn Spinner](https://github.com/YarnSpinnerTool/YarnEditor) - Tool for game writers to build branching dialogue and narratives for games.

### Public Domain Resources

*Please refer to the licensing information on each individual site for details.*

#### Colors
- [LOSPEC](https://lospec.com/) - Open source website with a community-built and searchable list of pixel art palettes, library of tutorials, and more.

#### Fonts
- [ForkAwesome](https://forkaweso.me/Fork-Awesome/) - Open source fork of "FontAwesome," a stellar font and CSS toolkit.
- [OpenDyslexic](https://opendyslexic.org/) - An accessible typeface for dyslexic readers.
- [TextVide](https://github.com/Gumball12/text-vide) - Open source reimplementation of "Bionic Reading," an accessibile font for neurodivergent (primarily ADHD/autistic) readers.

#### Game Assets
- [Kenney Assets](https://kenney.nl/) - Free pixel art game assets & sprite sheets under the Creative Commons 1.0 Universal license.
- [DevAssets](https://devassets.com/) - 2D and 3D game assets freely available for any project, commercial or not.
- [Quaternius' Assets](https://quaternius.itch.io/) - Free low poly, sometimes animated 3D models under CC0 license.
- [ambientCG](https://ambientcg.com/) - Free CC-1.0 Universal licensed resources for 3D modeling including textures, assets, and more.
- [OpenGameArt](https://opengameart.org/) - Primarily 2D pixel/isometric/low poly art, effect animations, and more.

#### Music / Sound
- [Audionautix](https://audionautix.com/) - Free production music for any use (even commercial) as long as credit is provided, under the Creative Commons Attribution 4.0 International License.
- [Creative Commons Jazz](https://jazz.mixremix.cc/) - It's exactly what it sounds like.
- [digCCMixter](http://dig.ccmixter.org/) - Another repository of free Creative Commons music intended for videos and games, credit to the original artist required.
- [OpenMetalCast](https://openmetalcast.com/) - No longer active, but a great repository of creative commons metal music.
- [Scott Buckley's Music Library](https://www.scottbuckley.com.au/) - Beautiful cinematic music released under CC-BY 4.0.
- [Twin Musicom Catalog](https://soundcloud.com/twinmusicom) - Free music in a variety of moods and genres under CC-BY 3.0 US. (Please note: their website is currently down, so I have linked to their SoundCloud for now!)
- [yoitrax's Royalty Free Music](https://yoitrax.bandcamp.com/) - Cool instrumental hip-hop under the Creative Commons Attribution 3.0 Unported license.

#### Textures
- [CGBookcase](https://www.cgbookcase.com/) - Free photorealistic PBR textures for 3D models, all under the Creative Commons 1.0 license.
- [Toptal's Subtle Patterns](https://www.toptal.com/designers/subtlepatterns/) - Creative Commons tilable pattern images for free.

#### Photography
- [Unsplash](https://unsplash.com/) - Free photography for that can be edited, manipulated, and reused in your projects.

#### Miscellaneous
- [Craig Maloney's List of Openly Licensed RPG Systems](https://decafbad.net/games/srds/) - A list of TTRPG systems that can be freely adapted however you would like.

## Media Sharing

*Places to publish your work!*

### Image Sharing

- [PixelFed](https://pixelfed.org/) - Fediverse image sharing platform (alternative to Instagram).

### Video Sharing

- [PeerTube](https://joinpeertube.org/) - Fediverse video sharing platform (alternative to YouTube).
- [OwnCast](https://owncast.online/) - Self-hostable livestreaming platform (alternative to Twitch).
- [Jellyfin](https://github.com/jellyfin/jellyfin) - Server and client pairing for streaming pre-recorded videos on demand, much like Plex.

### Audio Sharing

- [Castopod](https://castopod.org/) - Tool to self-host your own podcasts.
- [Rauversion](https://github.com/rauversion/rauversion-phx) - Open source, self-hostable music platform akin to Soundcloud.
- [Funkwhale](https://funkwhale.audio/) - Fediverse music sharing platform (alternative to GrooveShark).

## Maintainers

If you have questions or feedback regarding this list, then please create
an [Issue](https://codeberg.org/ADHDefy/delightful-creative-tools/issues) in our tracker, and optionally
`@mention` one or more of our maintainers:

- [`ADHDefy`](https://keyoxide.org/hkp/168FCC27B9BE809488674F6B6F93BFF9FF9DDD83) (codeberg: [@ADHDefy](https://codeberg.org/ADHDefy), fediverse: [@ADHDefy@easymode.im](https://easymode.im/@ADHDefy))

## License

[![CC0 Public domain. This work is free of known copyright restrictions.](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg)](https://creativecommons.org/publicdomain/zero/1.0/)

To the extent possible under law, the [maintainers](#maintainers) and other [contributors](delightful-contributors.md) have waived all copyright and related or neighboring rights to this work.