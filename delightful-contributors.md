# delightful contributors

These fine people brought us delight by adding their gems of freedom to the delight project.

> **Note**: This page is maintained by you, contributors. Please create a pull request or issue in our tracker if you contributed list entries and want to be included. [More info here](https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delight-us.md#attribution-of-contributors).

## We thank you for your gems of freedom :gem:

- [Arnold Schrijver](https://community.humanetech.com/u/aschrijver/summary) (codeberg: [@circlebuilder](https://codeberg.org/circlebuilder), fediverse: [@humanetech@mastodon.social](https://mastodon.social/@humanetech))
- [Yarmo Mackenbach](https://yarmo.eu/) (codeberg: [@yarmo](https://codeberg.org/yarmo), fediverse: [@yarmo@fosstodon.org](https://fosstodon.org/@yarmo))
- Chaihron (codeberg: [@chaihron](https://codeberg.org/chaihron), fediverse: [@chaihron@nixnet.social](https://nixnet.social/chaihron)
- [`ADHDefy`](https://keyoxide.org/hkp/168FCC27B9BE809488674F6B6F93BFF9FF9DDD83) (codeberg: [@ADHDefy](https://codeberg.org/ADHDefy), fediverse: [@ADHDefy@easymode.im](https://easymode.im/@ADHDefy))
- [lime360](https://lime360.neocities.org) (codeberg: [@lime360](https://codeberg.org/lime360), fediverse: [@lime360@mstdn.social](https://mstdn.social/@lime360))
- [Kartik Agaram](https://akkartik.name) (codeberg: [@akkartik](https://codeberg.org/akkartik), fediverse: [@akkartik@merveilles.town](https://merveilles.town/@akkartik))
